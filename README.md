# davraw9705

(deprecated repository) Converter for eddy covariance raw data files from CH-DAV 1997-2005

The source code for `davraw9705` can now be found here: [davraw9705 source code](https://gitlab.ethz.ch/flux/davraw9705)
